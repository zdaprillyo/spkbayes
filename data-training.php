<?php
include 'view/header.php';
include 'config/config.php';
?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Main content -->
    <section class="content container-fluid">
      
      <div class="row">

        <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Data Training</h3>  
              <p></p> 
              <a class="btn btn-success" href="data-training-tambah.php"><i class="glyphicon glyphicon-pencil"></i> Tambah Data</a>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-bordered">
                <thead>
                  <tr>
                  <th>No.</th>
                  <th>Nama Lokasi</th>
                    <?php
                      $c=mysqli_query($koneksi,"SELECT * FROM nbc_atribut order by id_atribut");
                      while($cl=mysqli_fetch_array($c)){
                    ?>
                      <th><?php echo $cl['atribut'];?></th>
                    <?php }?>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
                <?php
                    $no=1;
                    $t=mysqli_query($koneksi,"SELECT * FROM nbc_responden order by id_responden");
                    while($r=mysqli_fetch_array($t)){
                      ?>
                <tr>
                    <td><?php echo $no++;?></td>
                    <td><?php echo $r['responden'];?></td>
                    <?php
                      $cr=mysqli_query($koneksi,"SELECT * FROM nbc_responden JOIN nbc_atribut ON nbc_responden.id_atribut = nbc_atribut.id_atribut where responden = $r[responden] order by id_responden");
                      while($crl=mysqli_fetch_array($cr)){
                    ?>
                      <td><?php echo $cl['atribut'];?></td>
                    <?php }?>
                    <td>
                        <a href="data-training-edit.php?id_training=<?php echo $r['id_training'];?>" class="btn bg-indigo btn-xs waves-effect" title="Ubah"><i class="fa fa-edit"></i></a>                                                
                        <a href="data-training-hapus.php?id_training=<?php echo $r['id_training'];?>" class="btn bg-red btn-xs waves-effect" onclick="return confirm('Yakin Hapus Data')" title="Hapus"><i class="fa fa-trash"></i></a>
                    </td>
                </tr>
                    <?php } ?>
              </tbody></table>
            </div>
            <!-- /.box-body -->
          </div>
        </div>
      </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php
include 'view/footer.php';
?>