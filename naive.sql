-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 15, 2021 at 05:11 AM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `naive`
--

-- --------------------------------------------------------

--
-- Table structure for table `administrator`
--

CREATE TABLE IF NOT EXISTS `administrator` (
  `idadmin` int(11) NOT NULL AUTO_INCREMENT,
  `nama_admin` varchar(100) NOT NULL,
  `tempat_lahir` varchar(100) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `no_telpon` varchar(15) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(50) NOT NULL,
  PRIMARY KEY (`idadmin`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `administrator`
--

INSERT INTO `administrator` (`idadmin`, `nama_admin`, `tempat_lahir`, `tanggal_lahir`, `no_telpon`, `username`, `password`) VALUES
(2, 'Noor Malasari', 'Martapura', '1997-12-11', '082225009880', 'admin', '21232f297a57a5a743894a0e4a801fc3');

-- --------------------------------------------------------

--
-- Table structure for table `lokasi`
--

CREATE TABLE IF NOT EXISTS `lokasi` (
  `idlokasi` int(12) NOT NULL AUTO_INCREMENT,
  `namalokasi` varchar(100) DEFAULT NULL,
  `aksesibilitas` varchar(100) NOT NULL,
  `hargatanah` varchar(100) DEFAULT NULL,
  `kepadatan` varchar(100) DEFAULT NULL,
  `jarak` varchar(100) DEFAULT NULL,
  `status` varchar(30) DEFAULT NULL,
  `fasilitas` varchar(50) DEFAULT NULL,
  `sarana` varchar(50) DEFAULT NULL,
  `developer` varchar(40) DEFAULT NULL,
  `type` varchar(40) NOT NULL,
  PRIMARY KEY (`idlokasi`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=1653 ;

--
-- Dumping data for table `lokasi`
--

INSERT INTO `lokasi` (`idlokasi`, `namalokasi`, `aksesibilitas`, `hargatanah`, `kepadatan`, `jarak`, `status`, `fasilitas`, `sarana`, `developer`, `type`) VALUES
(1651, 'Guntung Manggis', 'Banyak', 'Mahal', 'Tinggi', 'Jauh', 'Lengkap', 'Lengkap', 'Lengkap', 'PT. Soka', 'Subsidi'),
(1652, 'Kurnia Landasan Ulin', 'Banyak', 'Mahal', 'Sangat Padat', 'Jauh', 'Lengkap', 'Lengkap', 'Lengkap', 'Lainnya', 'Subsidi');

-- --------------------------------------------------------

--
-- Table structure for table `nbc_atribut`
--

CREATE TABLE IF NOT EXISTS `nbc_atribut` (
  `id_atribut` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `atribut` varchar(100) NOT NULL,
  PRIMARY KEY (`id_atribut`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `nbc_atribut`
--

INSERT INTO `nbc_atribut` (`id_atribut`, `atribut`) VALUES
(1, 'Aksesibilitas'),
(2, 'Harga Tanah'),
(3, 'Kepadatan Penduduk'),
(4, 'Jarak'),
(5, 'Status'),
(6, 'Fasilitas'),
(7, 'Sarana');

-- --------------------------------------------------------

--
-- Table structure for table `nbc_data`
--

CREATE TABLE IF NOT EXISTS `nbc_data` (
  `id_data` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_responden` tinyint(3) unsigned NOT NULL,
  `id_atribut` tinyint(3) unsigned NOT NULL,
  `id_parameter` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`id_data`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `nbc_data`
--

INSERT INTO `nbc_data` (`id_data`, `id_responden`, `id_atribut`, `id_parameter`) VALUES
(1, 1, 1, 2),
(2, 1, 2, 2),
(3, 1, 3, 0),
(4, 1, 4, 2),
(5, 1, 5, 1),
(6, 1, 6, 1),
(7, 1, 7, 1),
(8, 2, 1, 2),
(9, 2, 2, 2),
(10, 2, 3, 3),
(11, 2, 4, 2),
(12, 2, 5, 1),
(13, 2, 6, 1),
(14, 2, 7, 1);

-- --------------------------------------------------------

--
-- Table structure for table `nbc_parameter`
--

CREATE TABLE IF NOT EXISTS `nbc_parameter` (
  `id_parameter` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `id_atribut` tinyint(3) unsigned NOT NULL,
  `nilai` tinyint(3) unsigned NOT NULL,
  `parameter` varchar(100) NOT NULL,
  PRIMARY KEY (`id_parameter`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

--
-- Dumping data for table `nbc_parameter`
--

INSERT INTO `nbc_parameter` (`id_parameter`, `id_atribut`, `nilai`, `parameter`) VALUES
(1, 1, 0, 'Kurang'),
(2, 1, 1, 'Cukup'),
(3, 1, 2, 'Banyak'),
(4, 2, 0, 'Murah'),
(5, 2, 1, 'Sedang'),
(6, 2, 2, 'Mahal'),
(7, 3, 0, 'Rendah'),
(8, 3, 1, 'Sedang'),
(9, 3, 2, 'Tinggi'),
(10, 3, 3, 'Sangat Padat'),
(11, 4, 0, 'Jauh'),
(12, 4, 1, 'Sedang'),
(13, 4, 2, 'Dekat'),
(14, 5, 0, 'Tidak Lengkap'),
(15, 5, 1, 'Lengkap'),
(16, 6, 0, 'Kurang Lengkap'),
(17, 6, 1, 'Cukup Lengkap'),
(18, 6, 2, 'Lengkap'),
(19, 7, 0, 'Kurang Lengkap'),
(20, 7, 1, 'Cukup Lengkap'),
(21, 7, 2, 'Lengkap');

-- --------------------------------------------------------

--
-- Table structure for table `nbc_responden`
--

CREATE TABLE IF NOT EXISTS `nbc_responden` (
  `id_responden` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `responden` varchar(50) DEFAULT NULL,
  `developer` varchar(40) NOT NULL,
  `type` varchar(40) NOT NULL,
  PRIMARY KEY (`id_responden`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=43 ;

--
-- Dumping data for table `nbc_responden`
--

INSERT INTO `nbc_responden` (`id_responden`, `responden`, `developer`, `type`) VALUES
(1, 'Guntung Manggis', '', ''),
(2, 'Kurnia Landasan Ulin', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `pimpinan`
--

CREATE TABLE IF NOT EXISTS `pimpinan` (
  `id_pimpinan` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  PRIMARY KEY (`id_pimpinan`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `pimpinan`
--

INSERT INTO `pimpinan` (`id_pimpinan`, `nama`) VALUES
(1, 'Nur Arif Rahman');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
