
<?php
session_start();
include 'view/header.php';
include 'config/config.php';

$idadmin=$_GET['idadmin'];
$t=mysqli_query($koneksi,"SELECT * FROM administrator WHERE idadmin='$idadmin'");
$r=mysqli_fetch_array($t);

if(isset($_POST['simpan'])){
  $nama_admin=$_POST['nama_admin'];
  $tempat_lahir=$_POST['tempat_lahir'];
  $tanggal_lahir=$_POST['tanggal_lahir'];
  $no_telpon=$_POST['no_telpon'];
  $username=$_POST['username'];
  $password = md5($_POST['password']);
    mysqli_query($koneksi,"UPDATE administrator SET nama_admin='$nama_admin',tempat_lahir='$tempat_lahir',tanggal_lahir='$tanggal_lahir',no_telpon='$no_telpon',username='$username' WHERE idadmin='$idadmin'");
  echo "
  <script>
      alert('Data Berhasil Diubah ! !');
      document.location.href = 'data-admin.php';
  </script>
";
}
?>
?>

   <!-- Content Wrapper. Contains page content -->
   <div class="content-wrapper">

<!-- Main content -->
<section class="content container-fluid">
  
  <div class="row">

    <div class="col-md-12">
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Ubah Data Pengguna</h3>  
        </div>
        <!-- /.box-header -->
        <div class="box-body">
        <form role="form" method="post" action="">
                                <div class="form-group row">
                                    <div class="col-md-3"></div>
                                    <div class="col-md-2">
                                        <label  class="textlabel">Nama Pengguna</label>
                                    </div>
                                     <div class="col-md-4">
                                      <input type="text" required="" name="nama_admin" value="<?php echo $r['nama_admin']; ?>" class="form-control">
                                     </div>
                                     <div class="col-md-3"></div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-md-3"></div>
                                    <div class="col-md-2">
                                        <label  class="textlabel">Tempat Lahir</label>
                                    </div>
                                     <div class="col-md-4">
                                      <input type="text" required="" name="tempat_lahir" value="<?php echo $r['tempat_lahir']; ?>" class="form-control">
                                     </div>
                                     <div class="col-md-3"></div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-md-3"></div>
                                    <div class="col-md-2">
                                        <label  class="textlabel">Tanggal Lahir</label>
                                    </div>
                                     <div class="col-md-4">
                                      <input type="date" required="" name="tanggal_lahir" value="<?php echo $r['tanggal_lahir']; ?>" class="form-control">
                                     </div>
                                     <div class="col-md-3"></div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-md-3"></div>
                                    <div class="col-md-2">
                                        <label  class="textlabel">No Telepon</label>
                                    </div>
                                     <div class="col-md-4">
                                      <input type="number " required="" name="no_telpon" value="<?php echo $r['no_telpon']; ?>" class="form-control">
                                     </div>
                                     <div class="col-md-3"></div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-md-3"></div>
                                    <div class="col-md-2">
                                        <label  class="textlabel">Username</label>
                                    </div>
                                    <div class="col-md-4">
                                      <input type="text" required="" name="username" value="<?php echo $r['username']; ?>" class="form-control">
                                     </div>
                                     <div class="col-md-3"></div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-md-3"></div>
                                    <div align="right" class="col-md-6"><button type="submit" name="simpan" class="btn btn-info">Simpan</button> <a class="btn btn-danger" href="data-admin.php">Batal</a>
                                    </div>
                                    
                                </div>
                            </form>   
             </div>
      </div>
    </div>
  </div>

</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->

<?php
include 'view/footer.php';
?>