
<?php
session_start();
include 'view/header.php';
include 'config/config.php';

if(isset($_POST['simpan'])){
  $nama_admin=$_POST['nama_admin'];
  $tempat_lahir=$_POST['tempat_lahir'];
  $tanggal_lahir=$_POST['tanggal_lahir'];
  $no_telpon=$_POST['no_telpon'];
  $username=$_POST['username'];
  $password = md5($_POST['password']);

  mysqli_query($koneksi,"INSERT INTO administrator(nama_admin,tempat_lahir,tanggal_lahir,no_telpon,username,password) VALUES('$nama_admin','$tempat_lahir','$tanggal_lahir','$no_telpon','$username','$password')");
  echo "
  <script>
      alert('Data Berhasil Ditambahkan ! !');
      document.location.href = 'data-admin.php';
  </script>
";
}
?>
?>

   <!-- Content Wrapper. Contains page content -->
   <div class="content-wrapper">

<!-- Main content -->
<section class="content container-fluid">
  
  <div class="row">

    <div class="col-md-12">
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Tambah Data Pengguna</h3>  
        </div>
        <!-- /.box-header -->
        <div class="box-body">
        <form role="form" method="post" action="">
                                <div class="form-group row">
                                    <div class="col-md-3"></div>
                                    <div class="col-md-2">
                                        <label  class="textlabel">Nama Pengguna</label>
                                    </div>
                                     <div class="col-md-4">
                                      <input type="text" required="" name="nama_admin" class="form-control">
                                     </div>
                                     <div class="col-md-3"></div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-md-3"></div>
                                    <div class="col-md-2">
                                        <label  class="textlabel">Tempat Lahir</label>
                                    </div>
                                     <div class="col-md-4">
                                      <input type="text" required="" name="tempat_lahir" class="form-control">
                                     </div>
                                     <div class="col-md-3"></div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-md-3"></div>
                                    <div class="col-md-2">
                                        <label  class="textlabel">Tanggal Lahir</label>
                                    </div>
                                     <div class="col-md-4">
                                      <input type="date" required="" name="tanggal_lahir" class="form-control">
                                     </div>
                                     <div class="col-md-3"></div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-md-3"></div>
                                    <div class="col-md-2">
                                        <label  class="textlabel">No Telepon</label>
                                    </div>
                                     <div class="col-md-4">
                                      <input type="number " required="" name="no_telpon" class="form-control">
                                     </div>
                                     <div class="col-md-3"></div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-md-3"></div>
                                    <div class="col-md-2">
                                        <label  class="textlabel">Username</label>
                                    </div>
                                    <div class="col-md-4">
                                      <input type="text" required="" name="username" class="form-control">
                                     </div>
                                     <div class="col-md-3"></div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-md-3"></div>
                                    <div class="col-md-2">
                                        <label  class="textlabel">Password</label>
                                    </div>
                                    <div class="col-md-4">
                                      <input type="password" name="password" required="" class="form-control" >
                                     </div>
                                     <div class="col-md-3"></div>
                                </div>
                               
                                
                                <div class="form-group row">
                                    <div class="col-md-3"></div>
                                    <div align="right" class="col-md-6"><button type="submit" name="simpan" class="btn btn-info">Simpan</button> <a class="btn btn-danger" href="data-admin.php">Batal</a>
                                    </div>
                                    
                                </div>
                            </form>   
             </div>
      </div>
    </div>
  </div>

</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->

<?php
include 'view/footer.php';
?>