<?php
include 'view/header.php';
include 'config/config.php';
?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Main content -->
    <section class="content container-fluid">
      
      <div class="row">

        <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Data Lokasi</h3>  
              <p></p> 
              <a class="btn btn-success" href="data-lokasi-tambah.php"><i class="glyphicon glyphicon-pencil"></i> Tambah Data</a>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-bordered">
                <thead>
                  <tr>
                  <th>No.</th>
                  <th>Nama Lokasi</th>
                  <th>Type</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
                <?php
                    $no=1;
                    $t=mysqli_query($koneksi,"SELECT * FROM nbc_responden ORDER BY id_responden;");
                    while($r=mysqli_fetch_array($t)){
                      ?>
                <tr>
                    <td><?php echo $no++;?></td>
                    <td><?php echo $r['responden'];?></td>
                    <td><?php echo $r['type'];?></td>
                    <td>
                        <a href="data-lokasi-edit.php?idlokasi=<?php echo $r['id_responden'];?>" class="btn bg-indigo btn-xs waves-effect" title="Ubah"><i class="fa fa-edit"></i></a>                                                
                        <a href="data-lokasi-hapus.php?idlokasi=<?php echo $r['id_responden'];?>" class="btn bg-red btn-xs waves-effect" onclick="return confirm('Yakin Hapus Data')" title="Hapus"><i class="fa fa-trash"></i></a>
                    </td>
                </tr>
                    <?php } ?>
              </tbody></table>
            </div>
            <!-- /.box-body -->
          </div>
        </div>
      </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php
include 'view/footer.php';
?>