
<?php
session_start();
include 'view/header.php';
include 'config/config.php';

$id_parameter=$_GET['id_parameter'];
$t=mysqli_query($koneksi,"SELECT * FROM nbc_parameter WHERE id_parameter='$id_parameter'");
$r=mysqli_fetch_array($t);

if(isset($_POST['simpan'])){
  $kriteria=$_POST['kriteria'];
  $parameter=$_POST['parameter'];
  $nilai=$_POST['nilai'];
  $keterangan=$_POST['keterangan'];

  mysqli_query($koneksi,"UPDATE nbc_parameter SET id_atribut='$kriteria',parameter='$parameter',nilai='$nilai',keterangan='$keterangan' WHERE id_parameter='$id_parameter'");
  echo "
  <script>
      alert('Data Berhasil Diubah ! !');
      document.location.href = 'data-parameter.php';
  </script>
";
}
?>
?>

   <!-- Content Wrapper. Contains page content -->
   <div class="content-wrapper">

<!-- Main content -->
<section class="content container-fluid">
  
  <div class="row">

    <div class="col-md-12">
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Ubah Data Parameter</h3>  
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <form role="form" method="post" action="">
                <div class="form-group row">
                    <div class="col-md-1"></div>
                                      <div class="col-md-2">
                                          <label  class="textlabel">Data Kriteria</label>
                                      </div>
                                      <div class="col-md-6">
                                      <select name="kriteria" class="form-control" required>
                                        <option value="">- Pilihan -</option>
                                        <?php
                                          $kriteria = mysqli_query($koneksi,"SELECT * FROM nbc_atribut ORDER BY id_atribut;");
                                          while ($pn = mysqli_fetch_array($kriteria)) {
                                              if ($pn['id_atribut']==$r['id_atribut']) {
                                                  $select2="selected";
                                                }else{
                                                  $select2="";
                                                }
                                          echo "<option value=$pn[id_atribut] $select2> $pn[atribut]</option>";
                                          }
                                        ?>
                                        </select>
                                      </div>
                                      <div class="col-md-2"></div>
                                  </div>
                                  <div class="form-group row">
                                    <div class="col-md-1"></div>
                                      <div class="col-md-2">
                                          <label  class="textlabel">Parameter</label>
                                      </div>
                                      <div class="col-md-6">
                                        <input type="text" required="" name="parameter" value="<?php echo $r['parameter'];?>" class="form-control">
                                      </div>
                                    <div class="col-md-2"></div>
                                  </div>
                                  <div class="form-group row">
                                    <div class="col-md-1"></div>
                                      <div class="col-md-2">
                                          <label  class="textlabel">Nilai</label>
                                      </div>
                                      <div class="col-md-6">
                                        <input type="number" required="" name="nilai" value="<?php echo $r['nilai'];?>" class="form-control">
                                      </div>
                                    <div class="col-md-2"></div>
                                  </div>
                                  <div class="form-group row">
                                    <div class="col-md-1"></div>
                                      <div class="col-md-2">
                                          <label  class="textlabel">Keterangan</label>
                                      </div>
                                      <div class="col-md-6">
                                        <input type="text" required="" name="keterangan" value="<?php echo $r['keterangan'];?>" class="form-control">
                                      </div>
                                    <div class="col-md-2"></div>
                                  </div>                                

                                <div class="form-group row">
                                    <div class="col-md-3"></div>
                                    <div align="right" class="col-md-6"><button type="submit" name="simpan" class="btn btn-info">Simpan</button> <a class="btn btn-danger" href="data-parameter.php">Batal</a>
                                    </div>
                                    
                                </div>
                            </form>   
             </div>
      </div>
    </div>
  </div>

</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->

<?php
include 'view/footer.php';
?>