
<?php
session_start();
include 'view/header.php';
include 'config/config.php';

if(isset($_POST['simpan'])){
  $namalokasi=$_POST['namalokasi'];
//   $aksesibilitas=$_POST['aksesibilitas'];
//   $harga=$_POST['harga'];
//   $kepadatan=$_POST['kepadatan'];
//   $jarak=$_POST['jarak'];
//   $status=$_POST['status'];
//   $fasilitas=$_POST['fasilitas'];
//   $sarana=$_POST['sarana'];
//   $developer=$_POST['developer'];
  $type=$_POST['type'];

  mysqli_query($koneksi,"INSERT INTO nbc_responden(responden,type) VALUES('$namalokasi','$type')");
  echo "
  <script>
      alert('Data Berhasil Ditambahkan ! !');
      document.location.href = 'data-lokasi.php';
  </script>
";
}
?>
?>

   <!-- Content Wrapper. Contains page content -->
   <div class="content-wrapper">

<!-- Main content -->
<section class="content container-fluid">
  
  <div class="row">

    <div class="col-md-12">
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Tambah Data Lokasi</h3>  
        </div>
        <!-- /.box-header -->
        <div class="box-body">
        <form role="form" method="post" action="">
                                <div class="form-group row">
                                    <div class="col-md-3"></div>
                                    <div class="col-md-2">
                                        <label  class="textlabel">Nama Lokasi</label>
                                    </div>
                                     <div class="col-md-4">
                                      <input type="text" required="" name="namalokasi" class="form-control">
                                     </div>
                                     <div class="col-md-3"></div>
                                </div>

                                <!-- <div class="form-group row">
                                    <div class="col-md-3"></div>
                                    <div class="col-md-2">
                                        <label  class="textlabel">Aksesibilitas</label>
                                    </div>
                                     <div class="col-md-6">
                                      <select name="aksesibilitas">
                                          <option value="">- Pilihan -</option>
                                          <option value="Banyak">3 atau lebih Jalur dari Pusat Kota (Banyak)</option>
                                          <option value="Cukup">2 Jalur dari Pusat Kota (Cukup)</option>
                                          <option value="Kurang">1 atau lebih Jalur dari Pusat Kota (Kurang)</option>
                                      </select>
                                     </div>
                                     <div class="col-md-3"></div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-md-3"></div>
                                    <div class="col-md-2">
                                        <label  class="textlabel">Harga Tanah</label>
                                    </div>
                                     <div class="col-md-6">
                                     <select name="harga">
                                          <option value="">- Pilihan -</option>
                                          <option value="Mahal">> Rp. 300.000/M2 (Mahal)</option>
                                          <option value="Sedang">< Rp. 300.000 - Rp. 150.000 (Sedang)</option>
                                          <option value="Murah">< Rp. 150.000 (Murah)</option>
                                      </select>
                                     </div>
                                     <div class="col-md-3"></div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-md-3"></div>
                                    <div class="col-md-2">
                                        <label  class="textlabel">Kepadatan Penduduk</label>
                                    </div>
                                     <div class="col-md-6">
                                     <select name="kepadatan">
                                          <option value="">- Pilihan -</option>
                                          <option value="Sangat Padat">> 400 Jiwa/ha (Sangat Padat)</option>
                                          <option value="Tinggi">201 - 400 Jiwa/ha (Tinggi)</option>
                                          <option value="Sedang">151 - 200 Jiwa/ha (Sedang)</option>
                                          <option value="Rendah">< 150 Jiwa/ha (Rendah)</option>
                                      </select>
                                     </div>
                                     <div class="col-md-3"></div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-md-3"></div>
                                    <div class="col-md-2">
                                        <label  class="textlabel">Jarak Dari Pusat Kota</label>
                                    </div>
                                    <div class="col-md-6">
                                    <select name="jarak">
                                          <option value="">- Pilihan -</option>
                                          <option value="Dekat">< 5 Km (Dekat)</option>
                                          <option value="Sedang">5 - 7 Km (Sedang)</option>
                                          <option value="Jauh">> 7 Km (Jauh)</option>
                                      </select>
                                     </div>
                                     <div class="col-md-3"></div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-md-3"></div>
                                    <div class="col-md-2">
                                        <label  class="textlabel">Status Tanah</label>
                                    </div>
                                    <div class="col-md-6">
                                    <select name="status">
                                          <option value="">- Pilihan -</option>
                                          <option value="Lengkap">SHM (Lengkap)</option>
                                          <option value="Tidak Lengkap">Sporadik atau tidak bersurat (Tidak Lengkap)</option>
                                      </select>
                                     </div>
                                     <div class="col-md-3"></div>
                                </div>
                               
                                <div class="form-group row">
                                    <div class="col-md-3"></div>
                                    <div class="col-md-2">
                                        <label  class="textlabel">Fasilitas Pelayanan</label>
                                    </div>
                                    <div class="col-md-6">
                                    <select name="fasilitas">
                                          <option value="">- Pilihan -</option>
                                          <option value="Lengkap">Lengkap</option>
                                          <option value="Cukup Lengkap">Cukup Lengkap</option>
                                          <option value="Kurang Lengkap">Kurang Lengkap</option>
                                      </select>
                                     </div>
                                     <div class="col-md-3"></div>
                                </div>
                               
                                <div class="form-group row">
                                    <div class="col-md-3"></div>
                                    <div class="col-md-2">
                                        <label  class="textlabel">Sarana dan Prasarana</label>
                                    </div>
                                    <div class="col-md-6">
                                    <select name="sarana">
                                          <option value="">- Pilihan -</option>
                                          <option value="Lengkap">Lengkap</option>
                                          <option value="Cukup Lengkap">Cukup Lengkap</option>
                                          <option value="Kurang Lengkap">Kurang Lengkap</option>
                                      </select>
                                     </div>
                                     <div class="col-md-3"></div>
                                </div>
 
                                <div class="form-group row">
                                    <div class="col-md-3"></div>
                                    <div class="col-md-2">
                                        <label  class="textlabel">Developer</label>
                                    </div>
                                    <div class="col-md-6">
                                    <select name="developer">
                                          <option value="">- Pilihan -</option>
                                          <option value="PT. Soka">PT. Soka</option>
                                          <option value="Lainnya">Lainnya</option>
                                      </select>
                                     </div>
                                     <div class="col-md-3"></div>
                                </div>
                                -->
                                <div class="form-group row">
                                    <div class="col-md-3"></div>
                                    <div class="col-md-2">
                                        <label  class="textlabel">Type</label>
                                    </div>
                                    <div class="col-md-6">
                                    <select name="type">
                                          <option value="">- Pilihan -</option>
                                          <option value="Subsidi">Subsidi</option>
                                          <option value="Non Subsidi">Non Subsidi</option>
                                      </select>
                                     </div>
                                     <div class="col-md-3"></div>
                                </div>
                               
                                
                                <div class="form-group row">
                                    <div class="col-md-3"></div>
                                    <div align="right" class="col-md-6"><button type="submit" name="simpan" class="btn btn-info">Simpan</button> <a class="btn btn-danger" href="data-lokasi.php">Batal</a>
                                    </div>
                                    
                                </div>
                            </form>   
             </div>
      </div>
    </div>
  </div>

</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->

<?php
include 'view/footer.php';
?>