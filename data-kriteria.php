<?php
include 'view/header.php';
include 'config/config.php';
?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Main content -->
    <section class="content container-fluid">
      
      <div class="row">

        <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Data Kriteria</h3>  
              <p></p> 
              <a class="btn btn-success" href="data-kriteria-tambah.php"><i class="glyphicon glyphicon-pencil"></i> Tambah Data</a>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-bordered">
                <thead>
                  <tr>
                  <th>No.</th>
                  <th>Nama Kriteria</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
                <?php
                    $no=1;
                    $t=mysqli_query($koneksi,"SELECT * FROM nbc_atribut ORDER BY id_atribut;");
                    while($r=mysqli_fetch_array($t)){
                      ?>
                <tr>
                    <td><?php echo $no++;?></td>
                    <td><?php echo $r['atribut'];?></td>
                    <td>
                        <a href="data-kriteria-edit.php?id_atribut=<?php echo $r['id_atribut'];?>" class="btn bg-indigo btn-xs waves-effect" title="Ubah"><i class="fa fa-edit"></i></a>                                                
                        <a href="data-kriteria-hapus.php?id_atribut=<?php echo $r['id_atribut'];?>" class="btn bg-red btn-xs waves-effect" onclick="return confirm('Yakin Hapus Data')" title="Hapus"><i class="fa fa-trash"></i></a>
                    </td>
                </tr>
                    <?php } ?>
              </tbody></table>
            </div>
            <!-- /.box-body -->
          </div>
        </div>
      </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php
include 'view/footer.php';
?>