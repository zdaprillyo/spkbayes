<?php
include 'view/header.php';
include 'config/config.php';

//-- konfigurasi database
$dbhost = 'localhost';
$dbuser = 'root';
$dbpass = '';
$dbname = 'naive';
//-- koneksi ke database server dengan extension mysqli
$db = new mysqli($dbhost,$dbuser,$dbpass,$dbname);
//-- hentikan program dan tampilkan pesan kesalahan jika koneksi gagal
if ($db->connect_error) {
    die('Connect Error ('.$db->connect_errno.')'.$db->connect_error);
}

//-- query untuk mendapatkan semua data atribut di tabel nbc_atribut
$sql = 'SELECT * FROM nbc_atribut';
$result = $db->query($sql);
//-- menyiapkan variable penampung berupa array
$atribut=array();
//-- melakukan iterasi pengisian array untuk tiap record data yang didapat
foreach ($result as $row) {
    $atribut[$row['id_atribut']]=$row['atribut'];
}
$jumlahatr = mysqli_query($koneksi,"SELECT * FROM nbc_atribut");
$jml_atribut = mysqli_num_rows($jumlahatr);


//-- query untuk mendapatkan semua data atribut di tabel nbc_atribut
$sql = 'SELECT * FROM nbc_parameter ORDER BY id_atribut,id_parameter';
$result = $db->query($sql);
//-- menyiapkan variable penampung berupa array
$parameter=array();
$id_atribut=0;
//-- melakukan iterasi pengisian array untuk tiap record data yang didapat
foreach ($result as $row) {
    if($id_atribut!=$row['id_atribut']){
        $parameter[$row['id_atribut']]=array();
        $id_atribut=$row['id_atribut'];
    }
    $parameter[$row['id_atribut']][$row['nilai']]=$row['parameter'];
}

?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Main content -->
    <section class="content container-fluid">
      
      <div class="row">

        <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Klasifikasi Naive Bayes</h3>  
              <p></p> 
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            <?php
                      //-- query untuk mendapatkan semua data training di tabel nbc_responden dan nbc_data
                      $sql = 'SELECT * FROM nbc_data a JOIN nbc_responden b USING(id_responden) ORDER BY a.id_data';
                      $result = $db->query($sql);
                      //-- menyiapkan variable penampung berupa array
                      $data=array();
                      $responden=array();
                      $id_responden=0;
                      //-- melakukan iterasi pengisian array untuk tiap record data yang didapat
                      foreach ($result as $row) {
                          if($id_responden!=$row['id_responden']){
                              $responden[$row['id_responden']]=$row['responden'];
                              $data[$row['id_responden']]=array();
                              $id_responden=$row['id_responden'];
                          }
                          $data[$row['id_responden']][$row['id_atribut']]=$row['id_parameter'];
                      }
                      //-- menampilkan data training dalam bentuk tabel
                      ?>
                      <table class="table table-bordered">
                      <caption>TABEL 1 : Data Training</caption>
                      <tr><th>Lokasi</th>
                      <?php 
                      //-- menampilkan header table
                      for($i=1;$i<=$jml_atribut;$i++){ 
                          echo "<th>{$atribut[$i]}</th>";
                      }
                      ?>
                      </tr>
                      <?php
                      //-- menampilkan data secara literal
                      foreach($data as $id_responden=>$dt_atribut){
                          echo "<tr><td>{$responden[$id_responden]}</td>";
                          for($i=1;$i<$jml_atribut;$i++){ 
                              echo "<td>{$parameter[$i][$dt_atribut[$i]]}</td>";
                          }
                          echo "<td>{$parameter[$i][$dt_atribut[$i]]}</td></tr>";
                      }
                      ?>
                      </table>

                      <?php
                      //-- menyiapkan variable penampung utk freq tiap atribut berupa array $freq
                      $rs=count($parameter);                      ;
                      $freq=array();
                      //-- inisialisasi data awal $freq
                      for($i=1;$i<=$jml_atribut;$i++){
                          for($j=0;$j<$jml_atribut;$j++){
                              for($k=0;$k<$jml_atribut;$k++){
                                  $freq[$i][$j][$k]=0;
                              }
                          }
                      }
                      //-- menyiapkan variable penampung utk freq prior berupa array $prior_freq
                      $prior_freq=array();
                      //-- iterasi tiap data training
                      foreach($data as $i=>$v){
                          //-- hitung freq tiap atribut
                          for($j=1;$j<=$jml_atribut;$j++){
                              $freq[$j][$v[$j]][$v[1]]+=1;
                          }
                          //-- hitung feq prior/kelas
                          if(!isset($prior_freq[$v[1]])) $prior_freq[$v[1]]=0;
                          $prior_freq[$v[1]]+=1;
                      }
                      ksort($prior_freq);
                      //-- menampilkan tabel frekuensi/jumlah data kelas
                      ?>
                      <!-- <table class="table table-bordered">
                      <caption>TABEL 2 : Jumlah Data Kelas <?php echo $atribut[1];?></caption>
                      <tr><?php foreach($parameter[1] as $nilai=>$param){ echo "<th>{$param}</th>"; }?></tr>
                      <tr><?php foreach($prior_freq as $nilai=>$nfreq){ echo "<td>{$nfreq}</td>"; }?></tr>
                      </table> -->
                      <?php 
                      //-- menampilkan tabel frekuensi/jumlah data tiap atribut

                      for($i=1;$i<=$jml_atribut;$i++){
                          echo "<table class='table table-bordered'> $rs";
                          //-- caption tabel utk masing-masing atribut
                          echo "<caption>TABEL ".($i+1)." : Jumlah Data Atribut {$atribut[$i]}</caption>";
                          echo "<tr><th rowspan='2'>{$atribut[$i]}</th><th colspan='{$rs}'>{$atribut[$i]}</th></tr><tr>"; 
                          //-- item nilai literal tiap atribut
                          foreach($parameter[$i] as $nilai=>$param){ 
                              echo "<th>{$param}</th>"; 
                          }
                          echo "</tr>";
                          //-- iterasi utk tiap nilai kelas
                          foreach($parameter[$i] as $n=>$p){
                              echo "<tr><td>{$p}</td>";
                              //-- iterasi jumlah data/freq tiap nilai atribut
                              for($j=0;$j<=3;$j++){
                              echo "<td>{$freq[$i][$j][$n]}</td>";
                              }
                              echo "</tr>";
                          }
                          echo "</table>";
                      }
                      ?>

                        <?php
                        //-- menyiapkan variable penampung untuk class probabilities $prior
                        $prior=array();
                        //-- hitung nilai masing2 class probabilities
                        foreach($prior_freq as $p=>$v){
                            $prior[$p]=$v/array_sum($prior_freq);
                        }
                        ksort($prior);
                        ?>
                        <!-- <table class="table table-bordered"> 
                        <caption>TABEL 9 : Probabilitas Kelas <?php echo $atribut[$jml_atribut];?></caption> 
                        <tr><?php foreach($parameter[$jml_atribut] as $nilai=>$param){ echo "<th>{$param}</th>"; }?></tr> 
                        <tr><?php foreach($prior as $nilai=>$nprior){ echo "<td>{$nprior}</td>"; }?></tr> 
                        </table>  -->

                    <?php
                    //-- menyiapkan variabel penampung probabilitas per data training thd kelas
                    $prob=array();
                    //-- inisialisasi jumlah kumulatif prediksi benar dari data training
                    $right=0;
                    //-- iterasi utk setiap data training
                    foreach($data as $n=>$v){
                        $m=array();
                        //-- iterasi utk setiap nilai kelas
                        for($i=0;$i<3;$i++){
                            //-- inisialisasi probabilitas awal
                            $m[$i]=1;
                            //-- perkalian nilai probabilitas tiap atribut
                            for($j=2;$j<=$jml_atribut;$j++){
                                $m[$i]*=$likehood[$j][$v[$j]][$i];
                            }
                            //-- kalikan dengan prior probabilitasnya
                            $m[$i]*=$prior[$i];
                        }
                        //-- menentukan nilai prediksi kelas per data training
                        $predict[$n]=array_search(max($m),$m);
                        $prob[$n]=$m;
                        //-- hitung kumulatif prediksi yang benar
                        $right+=($predict[$n]==$v[1]?1:0);
                    }
                    //-- menampilkan prosentase data training yg diprediksi benar
                    echo "Correctly Classified Instance = ".($right/count($data)*100)."% <br>";
                    //-- menampilkan prosentase data training yg diprediksi tidak benar
                    echo "Incorrectly Classified Instance = ".((1-$right/count($data))*100)."% <br>";
                    ?>
            </div>
            <!-- /.box-body -->
          </div>
        </div>
      </div>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php
include 'view/footer.php';
?>