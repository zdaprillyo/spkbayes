<?php 
class Database{
	public $link;
	public $error;

	public function __construct(){	
		$this->connectDB();
	}

	private function connectDB(){
		$this->link = new mysqli(DB_HOST,DB_USER,DB_PASS,DB_NAME);
		if(!$this->link){
			$this->error = "Connected fail".$this->link->connect_error;
			return false;
		}
	}

	//select or read database
	public function select($query){
		$result = $this->link->query($query) or die($this->link->error.__LINE__);
		if($result){
			return $result;
		}else{
			return false;
		}
	}

	public function insert($query){
		$insert_row = $this->link->query($query) or die($this->link->error.__LINE__);
		return $insert_row;
	}

	public function delete($query){
		$delete_row = $this->link->query($query) or die($this->link->error.__LINE__);
		return $delete_row;
	}

	public function update($query){
		$update_row = $this->link->query($query) or die($this->link->error.__LINE__);
		return $update_row;
	}

	public function login($query){
		$result = $this->link->query($query) or die($this->link->error.__LINE__);
		if($result){
			return $result;
		}else{
			return false;
		}	
	}
}

?>