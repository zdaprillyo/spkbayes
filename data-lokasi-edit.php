
<?php
session_start();
include 'view/header.php';
include 'config/config.php';

$idlokasi=$_GET['idlokasi'];
$t=mysqli_query($koneksi,"SELECT * FROM nbc_responden WHERE id_responden='$idlokasi'");
$r=mysqli_fetch_array($t);

if(isset($_POST['simpan'])){
  $namalokasi=$_POST['namalokasi'];
//   $aksesibilitas=$_POST['aksesibilitas'];
//   $harga=$_POST['harga'];
//   $kepadatan=$_POST['kepadatan'];
//   $jarak=$_POST['jarak'];
//   $status=$_POST['status'];
//   $fasilitas=$_POST['fasilitas'];
//   $sarana=$_POST['sarana'];
//   $developer=$_POST['developer'];
  $type=$_POST['type'];

  mysqli_query($koneksi,"UPDATE nbc_responden SET responden='$namalokasi',type='$type' WHERE id_responden='$id_responden'");
  echo "
  <script>
      alert('Data Berhasil Diubah ! !');
      document.location.href = 'data-lokasi.php';
  </script>
";
}
?>
?>

   <!-- Content Wrapper. Contains page content -->
   <div class="content-wrapper">

<!-- Main content -->
<section class="content container-fluid">
  
  <div class="row">

    <div class="col-md-12">
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Ubah Data Lokasi</h3>  
        </div>
        <!-- /.box-header -->
        <div class="box-body">
        <form role="form" method="post" action="">
        <div class="form-group row">
                                    <div class="col-md-3"></div>
                                    <div class="col-md-2">
                                        <label  class="textlabel">Nama Lokasi</label>
                                    </div>
                                     <div class="col-md-4">
                                      <input type="text" required="" name="namalokasi" value="<?php echo $r['responden'];?>" class="form-control">
                                     </div>
                                     <div class="col-md-3"></div>
                                </div>

                                <!-- <div class="form-group row">
                                    <div class="col-md-3"></div>
                                    <div class="col-md-2">
                                        <label  class="textlabel">Aksesibilitas</label>
                                    </div>
                                     <div class="col-md-6">
                                      <select name="aksesibilitas">
                                          <option value="">- Pilihan -</option>
                                          <option value="Banyak" <?php if($r['aksesibilitas']=='Banyak') echo 'selected'?>>3 atau lebih Jalur dari Pusat Kota (Banyak)</option>
                                          <option value="Cukup" <?php if($r['aksesibilitas']=='Cukup') echo 'selected'?>>2 Jalur dari Pusat Kota (Cukup)</option>
                                          <option value="Kurang" <?php if($r['aksesibilitas']=='Kurang') echo 'selected'?>>1 atau lebih Jalur dari Pusat Kota (Kurang)</option>
                                      </select>
                                     </div>
                                     <div class="col-md-3"></div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-md-3"></div>
                                    <div class="col-md-2">
                                        <label  class="textlabel">Harga Tanah</label>
                                    </div>
                                     <div class="col-md-6">
                                     <select name="harga">
                                          <option value="">- Pilihan -</option>
                                          <option value="Mahal" <?php if($r['hargatanah']=='Mahal') echo 'selected'?>> Rp. 300.000/M2 (Mahal)</option>
                                          <option value="Sedang" <?php if($r['hargatanah']=='Sedang') echo 'selected'?>> < Rp. 300.000 - Rp. 150.000 (Sedang)</option>
                                          <option value="Murah" <?php if($r['hargatanah']=='Murah') echo 'selected'?>> < Rp. 150.000 (Murah)</option>
                                      </select>
                                     </div>
                                     <div class="col-md-3"></div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-md-3"></div>
                                    <div class="col-md-2">
                                        <label  class="textlabel">Kepadatan Penduduk</label>
                                    </div>
                                     <div class="col-md-6">
                                     <select name="kepadatan">
                                          <option value="">- Pilihan -</option>
                                          <option value="Sangat Padat" <?php if($r['kepadatan']=='Sangat Padat') echo 'selected'?>>> 400 Jiwa/ha (Sangat Padat)</option>
                                          <option value="Tinggi" <?php if($r['kepadatan']=='Tinggi') echo 'selected'?>>201 - 400 Jiwa/ha (Tinggi)</option>
                                          <option value="Sedang" <?php if($r['kepadatan']=='Sedang') echo 'selected'?>>151 - 200 Jiwa/ha (Sedang)</option>
                                          <option value="Rendah" <?php if($r['kepadatan']=='Rendah') echo 'selected'?>>< 150 Jiwa/ha (Rendah)</option>
                                      </select>
                                     </div>
                                     <div class="col-md-3"></div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-md-3"></div>
                                    <div class="col-md-2">
                                        <label  class="textlabel">Jarak Dari Pusat Kota</label>
                                    </div>
                                    <div class="col-md-6">
                                    <select name="jarak">
                                      
                                          <option value="">- Pilihan -</option>
                                          <option value="Dekat" <?php if($r['jarak']=='Dekat') echo 'selected'?>>< 5 Km (Dekat)</option>
                                          <option value="Sedang" <?php if($r['jarak']=='Sedang') echo 'selected'?>>5 - 7 Km (Sedang)</option>
                                          <option value="Jauh" <?php if($r['jarak']=='Jauh') echo 'selected'?>>> 7 Km (Jauh)</option>
                                      </select>
                                     </div>
                                     <div class="col-md-3"></div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-md-3"></div>
                                    <div class="col-md-2">
                                        <label  class="textlabel">Status Tanah</label>
                                    </div>
                                    <div class="col-md-6">
                                    <select name="status">
                                          <option value="">- Pilihan -</option>
                                          <option value="Lengkap" <?php if($r['status']=='Lengkap') echo 'selected'?>>SHM (Lengkap)</option>
                                          <option value="Tidak Lengkap" <?php if($r['status']=='Tidak Lengkap') echo 'selected'?>>Sporadik atau tidak bersurat (Tidak Lengkap)</option>
                                      </select>
                                     </div>
                                     <div class="col-md-3"></div>
                                </div>
                               
                                <div class="form-group row">
                                    <div class="col-md-3"></div>
                                    <div class="col-md-2">
                                        <label  class="textlabel">Fasilitas Pelayanan</label>
                                    </div>
                                    <div class="col-md-6">
                                    <select name="fasilitas">
                                          <option value="">- Pilihan -</option>
                                          <option value="Lengkap" <?php if($r['fasilitas']=='Lengkap') echo 'selected'?>>Lengkap</option>
                                          <option value="Cukup Lengkap" <?php if($r['fasilitas']=='Cukup Lengkap') echo 'selected'?>>Cukup Lengkap</option>
                                          <option value="Kurang Lengkap" <?php if($r['fasilitas']=='Kurang Lengkap') echo 'selected'?>>Kurang Lengkap</option>
                                      </select>
                                     </div>
                                     <div class="col-md-3"></div>
                                </div>
                               
                                <div class="form-group row">
                                    <div class="col-md-3"></div>
                                    <div class="col-md-2">
                                        <label  class="textlabel">Sarana dan Prasarana</label>
                                    </div>
                                    <div class="col-md-6">
                                    <select name="sarana">
                                          <option value="">- Pilihan -</option>
                                          <option value="Lengkap" <?php if($r['sarana']=='Lengkap') echo 'selected'?>>Lengkap</option>
                                          <option value="Cukup Lengkap" <?php if($r['sarana']=='Cukup Lengkap') echo 'selected'?>>Cukup Lengkap</option>
                                          <option value="Kurang Lengkap" <?php if($r['sarana']=='Kurang Lengkap') echo 'selected'?>>Kurang Lengkap</option>
                                       </select>
                                     </div>
                                     <div class="col-md-3"></div>
                                </div>
 
                                <div class="form-group row">
                                    <div class="col-md-3"></div>
                                    <div class="col-md-2">
                                        <label  class="textlabel">Developer</label>
                                    </div>
                                    <div class="col-md-6">
                                    <select name="developer">
                                          <option value="">- Pilihan -</option>
                                          <option value="PT. Soka" <?php if($r['developer']=='PT. Soka') echo 'selected'?>>PT. Soka</option>
                                          <option value="Lainnya" <?php if($r['developer']=='Lainnya') echo 'selected'?>>Lainnya</option>
                                      </select>
                                     </div>
                                     <div class="col-md-3"></div>
                                </div> -->
                               
                                <div class="form-group row">
                                    <div class="col-md-3"></div>
                                    <div class="col-md-2">
                                        <label  class="textlabel">Type</label>
                                    </div>
                                    <div class="col-md-6">
                                    <select name="type">
                                          <option value="">- Pilihan -</option>
                                          <option value="Subsidi" <?php if($r['type']=='Subsidi') echo 'selected'?>>Subsidi</option>
                                          <option value="Non Subsidi" <?php if($r['type']=='Non Subsidi') echo 'selected'?>>Non Subsidi</option>
                                      </select>
                                     </div>
                                     <div class="col-md-3"></div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-md-3"></div>
                                    <div align="right" class="col-md-6"><button type="submit" name="simpan" class="btn btn-info">Simpan</button> <a class="btn btn-danger" href="data-lokasi.php">Batal</a>
                                    </div>
                                    
                                </div>
                            </form>   
             </div>
      </div>
    </div>
  </div>

</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->

<?php
include 'view/footer.php';
?>